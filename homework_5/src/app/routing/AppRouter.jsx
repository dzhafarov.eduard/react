import React from 'react';
import {Route, Switch} from "react-router-dom";
import MainPage from "../pages/MainPage";
import FavoritesPage from "../pages/FavoritesPage";
import CartPage from "../pages/CartPage";

const AppRouter = () => {
	return (
		<Switch>
			<Route path='/' exact component={MainPage}/>
			<Route path='/favorites' exact component={FavoritesPage}/>
			<Route path='/cart' exact component={CartPage}/>
		</Switch>
	)
}

export default AppRouter