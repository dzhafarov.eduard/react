const initStore = {
	items: [],
	favorites: [],
	cart: [],
	modal: null
}

export default initStore