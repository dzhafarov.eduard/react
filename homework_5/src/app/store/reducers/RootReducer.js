import {combineReducers} from "redux";
import itemsReducer from './ItemsReducer'
import cartReducer from "./CartReducer";
import favoritesReducer from "./FavoritesReducer";
import modalReducer from "./ModalReducer";

const rootReducer = combineReducers({
	items: itemsReducer,
	cart: cartReducer,
	favorites: favoritesReducer,
	modal: modalReducer
})

export default rootReducer