import React from 'react';

const Button = (
	{
		text,
		backgroundColor,
		onClick
	}
) => <button style={{background: backgroundColor}} onClick={onClick}>{text}</button>

export default Button;