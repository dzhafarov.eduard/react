import React from 'react';
import './Modal.scss'

const Modal = (
	{
		header,
		text,
		closeButton,
		actions,
		handleClose
	}
) => {

	return (
		<div className='modal__background'
		     onClick={(event) => event.currentTarget === event.target && handleClose()}>
			<div className='modal'>
				<div className='modal__header'>
					<h5>{header}</h5>
					{closeButton && <span className='modal__close' onClick={handleClose}/>}
				</div>
				<div className='modal__content'>
					<p>{text}</p>
				</div>
				<div className='modal__actions'>
					{actions}
				</div>
			</div>
		</div>
	)
}

export default Modal;