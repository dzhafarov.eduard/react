import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {openAddToCartModal} from "../../../store/actions/ModalActions";
import {addToFavorites, removeFromFavorites} from "../../../store/actions/FavoritesActions";
import Button from "../../Button/Button";
import StarIcon from "../../../assets/icons/StarIcon";
import "./ItemCard.scss"

const ItemCard = ({item}) => {

	const {img, title, article, color, price} = item

	const dispatch = useDispatch()
	const favorites = useSelector(store => store.favorites)

	const isFavorite = () => favorites.some(i => i.article === article)

	const handleFavorite = () => {
		isFavorite()
			? dispatch(removeFromFavorites(item))
			: dispatch(addToFavorites(item))
	}

	return (
		<div className='item-card'>
			<div className='item-card__img-wrapper'>
				<img className='item-card__img' src={img} alt={title}/>
			</div>
			<div className='item-card__content'>
				<h5 className='item-card__title'>{title}</h5>
				<p className='item-card__article'>Article: {article}</p>
				<p className='item-card__color'>Color: {color}</p>
			</div>
			<div className='item-card__footer'>
				<span className='item-card__price'>{price} UAH</span>
				<div className='item-card__actions'>
					<Button text='Add to cart' onClick={() => dispatch(openAddToCartModal(item))}/>
					<span className='item-card__actions__fav' onClick={() => handleFavorite()}>
						{StarIcon(isFavorite() ? '#ffc107' : '#ffffff')}
					</span>
				</div>
			</div>
		</div>
	)
}

export default ItemCard;