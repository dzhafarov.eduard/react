import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {ErrorMessage, Field, Form, Formik} from "formik";
import {number, object, string} from "yup";
import {cleanCart} from "../../../store/actions/CartActions";
import "./CheckoutForm.scss"

const CheckoutForm = () => {

	const dispatch = useDispatch()
	const cart = useSelector(store => store.cart)

	const initialValues = {
		name: '',
		surname: '',
		age: '',
		address: '',
		phone: ''
	}

	const submitHandle = (values) => new Promise(res => {
		setTimeout(() => {
			dispatch(cleanCart())
			console.log({
				order: cart,
				userData: values
			})
			res()
		}, 1000)
	})

	const validationSchema = object({
		name: string()
			.min(3)
			.max(20)
			.required(),
		surname: string()
			.min(3)
			.max(20)
			.required(),
		age: number()
			.min(1, 'Incorrect input! Please, enter your age!')
			.max(100, 'Incorrect input! Please, enter your age!')
			.required(),
		address: string()
			.required(),
		phone: string()
			.length(10)
			.required()
	})

	return (
		<Formik initialValues={initialValues} onSubmit={submitHandle} validationSchema={validationSchema}>
			{
				({isSubmitting, isValid}) => <Form className='form-checkout'>
					<Field className='form-checkout__input' name='name' placeholder='Name'/>
					<ErrorMessage name='name'/>
					<Field className='form-checkout__input' name='surname' placeholder='Surname'/>
					<ErrorMessage name='surname'/>
					<Field className='form-checkout__input' name='age' placeholder='Age'/>
					<ErrorMessage name='age'/>
					<Field className='form-checkout__input' name='address' placeholder='Address'/>
					<ErrorMessage name='address'/>
					<Field className='form-checkout__input' name='phone' type='tel' placeholder='Phone 0XXXXXXXXX'/>
					<ErrorMessage name='phone'/>
					<button className='form-checkout__submit'
					        type='submit'
					        disabled={isSubmitting || !isValid}
					>Checkout
					</button>
				</Form>
			}
		</Formik>
	);
};

export default CheckoutForm;
