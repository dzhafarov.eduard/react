import {applyMiddleware, compose, createStore} from "redux";
import rootReducer from "./redusers/RootReducer";
import initStore from "./InitStore";
import thunk from "redux-thunk";

const store = createStore(
	rootReducer,
	initStore,
	compose(
		applyMiddleware(thunk),
		window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
	)
)

export default store