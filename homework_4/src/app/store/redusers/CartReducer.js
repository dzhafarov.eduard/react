import {
	ADD_TO_CART,
	CART_LOADING,
	DECR_COUNT_IN_CART,
	INCR_COUNT_IN_CART,
	REMOVE_FROM_CART
} from "../actions/CartActions";
import initStore from "../InitStore";

const cartReducer = (store = initStore.cart, {type, cart}) => {
	switch (type) {
		case CART_LOADING:
			return cart
		case ADD_TO_CART:
			return cart
		case REMOVE_FROM_CART:
			return cart
		case INCR_COUNT_IN_CART:
			return cart
		case DECR_COUNT_IN_CART:
			return cart
		default:
			return store
	}
}

export default cartReducer