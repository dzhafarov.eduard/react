import {ITEMS_LOADED} from "../actions/ItemsActions";
import initStore from "../InitStore";

const itemsReducer = (store = initStore.items, action) => {
	switch (action.type) {
		case ITEMS_LOADED: return action.items
		default: return store
	}
}

export default itemsReducer