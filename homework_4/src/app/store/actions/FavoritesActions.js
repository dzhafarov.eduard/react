import {getFromLocalStorage, putToLocalStorage} from "../../utils/Utils";

export const LOAD_FAVORITES = "LOAD_FAVORITES"
export const ADD_TO_FAVORITES = "ADD_TO_FAVORITES"
export const REMOVE_FROM_FAVORITES = "REMOVE_FROM_FAVORITES"

export const loadFavorites = () => dispatch => {
	const items = getFromLocalStorage('favorites', [])
	dispatch({
		type: LOAD_FAVORITES,
		items
	})
}

export const addToFavorites = item => dispatch => {
	const items = getFromLocalStorage('favorites', [])
	items.push(item)
	putToLocalStorage('favorites', items)
	dispatch({
		type: ADD_TO_FAVORITES,
		items
	})
}

export const removeFromFavorites = item => dispatch => {
	let items = getFromLocalStorage('favorites', [])
	items = items.filter(i => i.article !== item.article)
	putToLocalStorage('favorites', items)
	dispatch({
		type: REMOVE_FROM_FAVORITES,
		items
	})
}