export const putToLocalStorage = (key, value) => {
	localStorage.setItem(key, JSON.stringify(value))
}

export const getFromLocalStorage = (key, init) => {
	return JSON.parse(localStorage.getItem(key)) || init
}