import React from 'react';
import {useSelector} from "react-redux";
import CartItem from "./components/CartItem";
import "./CartContainer.scss"

const CartContainer = () => {

	const cart = useSelector(store => store.cart)

	const totalPrice = cart.length > 0 ? cart.map(item => item.price).reduce((p,n) => p + n) : 0
	const uniqueArticles = cart.map(item => item.article).filter((article, index, arr) => arr.indexOf(article) === index)

	const itemCards = uniqueArticles.map(
		(article, index) => {
			const item = cart.find(item => item.article === article)
			return <CartItem key={(index + 1) * item.id * Math.random()} item={item}/>
		}
	)

	return (
		<div className='cart-container'>
			{itemCards.length > 0
				? itemCards
				: <p className='cart-container__message'>Your cart is empty</p>
			}
			<p className='cart-total-price'>{`Total price: ${totalPrice} UAH`}</p>
		</div>
	)
}

export default CartContainer