import React from 'react';
import {useSelector} from "react-redux";
import CartIcon from "../../../assets/icons/CartIcon";
import "./CartMenuItem.scss"

const CartMenuItem = () => {


	const cart = useSelector(store => store.cart)

	return (
		<div className='navbar__menu__cart'>
			<span>Cart</span>
			{CartIcon()}
			{cart.length > 0 && <span className='navbar__menu__cart-badge'>{cart.length}</span>}
		</div>
	);
}

export default CartMenuItem