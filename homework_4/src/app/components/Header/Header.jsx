import React from 'react';
import {Link} from "react-router-dom";
import CartMenuItem from "./components/CartMenuItem";
import './Header.scss'

const Header = () => {

	return (
		<header>
			<div className='container'>
				<nav className='navbar'>
					<a className='navbar__logo' href="/">Some Logo</a>
					<menu className='navbar__menu'>
						<li className='navbar__menu__item'>
							<Link to='/'>Main</Link>
						</li>
						<li className='navbar__menu__item'>
							<Link to='/favorites'>Favorites</Link>
						</li>
						<li className='navbar__menu__item'>
							<Link to='/cart'>
								<CartMenuItem/>
							</Link>
						</li>
					</menu>
				</nav>
			</div>
		</header>
	);
}

export default Header;