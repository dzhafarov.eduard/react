import React, {Component} from 'react';
import Header from "./components/Header/Header";
import ItemsContainer from "./components/ItemsContainer/ItemsContainer";
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/Button";
import axios from 'axios'
import './App.scss';

class App extends Component {
	state = {
		items: [],
		cart: [],
		modal: null
	}

	componentDidMount() {
		axios.get('./items.json')
			.then(res => this.setState({items: res.data}))
		this.setState({
			cart: JSON.parse(localStorage.getItem('cart')) || []
		})
	}

	addToCart(item) {
		const {cart} = this.state
		cart.push(item)
		localStorage.setItem('cart', JSON.stringify(cart))
		this.setState({cart, modal: null})
	}

	handleModal(item) {
		this.setState({
			modal: <Modal
				header='Add to cart'
				text='Do you want add this item to the cart?'
				closeButton={false}
				actions={<div>
					<Button text='ok' onClick={() => this.addToCart(item)}/>
					<Button text='Cancel' onClick={() => this.setState({modal: null})}/>
				</div>}
				handleClose={() => this.setState({modal: null})}
			/>
		})
	}

	render() {
		const {items, cart, modal} = this.state
		return (
			<div className="App">
				<Header cart={cart}/>
				<ItemsContainer items={items} addToCart={(item) => this.handleModal(item)}/>
				{modal}
			</div>
		);
	}
}

export default App;
