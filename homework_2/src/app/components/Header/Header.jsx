import React, {Component} from 'react';
import Cart from "../Cart/Cart";
import './Header.scss'

class Header extends Component {
	render() {
		const {cart} = this.props
		return (
			<header>
				<div className='container'>
					<nav className='navbar row flex-between'>
						<a className='logo' href="/">Some Logo</a>
						<Cart cart={cart}/>
					</nav>
				</div>
			</header>
		);
	}
}

export default Header;