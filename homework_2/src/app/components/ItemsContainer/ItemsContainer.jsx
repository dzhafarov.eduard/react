import React, {Component} from 'react';
import Item from "../Item/Item";
import './ItemsContainer.scss'

class ItemsContainer extends Component {

	render() {
		const {items, addToCart} = this.props
		const renderItems = items.map(item => <Item key={item.article} item={item} addToCart={addToCart}/>)
		return (
			<div className='container'>
				<div className='items-container'>
					{renderItems}
				</div>
			</div>
		);
	}
}

export default ItemsContainer;