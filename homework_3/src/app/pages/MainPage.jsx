import React from 'react';
import ItemsContainer from "../components/ItemsContainer/ItemsContainer";

const MainPage = (props) => {

	const {items, addToCart, handleFavorites} = props

	return (
		<main>
			<div className="container">
				<ItemsContainer items={items} addToCart={addToCart} handleFavorites={handleFavorites}/>
			</div>
		</main>
	);
}

export default MainPage;