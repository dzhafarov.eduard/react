import React, {useEffect, useState} from 'react';
import Button from "../../Button/Button";
import {starIcon} from "../../../assets/AppIcons";
import {fromLocalStorage} from "../../../utils/Utils";
import "./CartItem.scss"

const CartItem = (props) => {

	const {
		item,
		count,
		removeFromCart,
		handleFavorites,
		incrementItemCountInCart,
		decrementItemCountInCart
	} = props

	const {img, title, article, color, price} = item

	const [isFavorite, setIsFavorite] = useState(false)


	useEffect(() => {
		const favs = fromLocalStorage('favorites', [])
		setIsFavorite(favs.some(itm => itm.article === article))
	}, [])

	return (
		<div className='cart-item'>
			<div className='cart-item__img-wrapper'>
				<img className='cart-item__img' src={img} alt={title}/>
			</div>
			<div className='cart-item__content'>
				<h5 className='cart-item__title'>{title}</h5>
				<p className='cart-item__article'>Article: {article}</p>
				<p className='cart-item__color'>Color: {color}</p>
			</div>
			<div className='cart-item__actions'>
				<div>
					<button className='cart-item__count-change-btn'
					        onClick={() => decrementItemCountInCart(item)}
					>-
					</button>
					<span className='cart-item__count'>{count}</span>
					<button className='cart-item__count-change-btn'
					        onClick={() => incrementItemCountInCart(item)}
					>+
					</button>
				</div>
				<span className='cart-item__price'>{price * count} UAH</span>
				<Button text='Remove' onClick={() => removeFromCart(item)}/>
				<span className='cart-item__actions__fav' onClick={() => {
					const isFav = handleFavorites(item)
					setIsFavorite(isFav)
				}}>
                        {starIcon(isFavorite ? '#ffc107' : '#ffffff')}
                    </span>
			</div>
		</div>
	)
}

export default CartItem;