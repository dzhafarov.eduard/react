import React, {Component} from 'react';
import PropTypes from 'prop-types'

class Button extends Component {
	static propTypes = {
		text: PropTypes.string,
		backgroundColor: PropTypes.string,
		onClick: PropTypes.func,
	}

	static defaultProps = {
		text: 'Button',
		backgroundColor: '#fffff'
	}

	render() {
		const {text, backgroundColor, onClick} = this.props
		return (
			<button style={{background: backgroundColor}}
			        onClick={onClick}
			>
				{text}
			</button>
		);
	}
}

export default Button;