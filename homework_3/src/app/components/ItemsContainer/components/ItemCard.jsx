import React, {useEffect, useState} from 'react';
import Button from "../../Button/Button";
import {starIcon} from "../../../assets/AppIcons";
import {fromLocalStorage} from "../../../utils/Utils";
import "./ItemCard.scss"

const ItemCard = (props) => {

	const {item, addToCart, handleFavorites} = props
	const {img, title, article, color, price} = item
	const [isFavorite, setIsFavorite] = useState(false)

	useEffect(() => {
		const favs = fromLocalStorage('favorites', [])
		setIsFavorite(favs.some(itm => itm.article === article))
	}, [])

	return (
		<div className='item-card'>
			<div className='item-card__img-wrapper'>
				<img className='item-card__img' src={img} alt={title}/>
			</div>
			<div className='item-card__content'>
				<h5 className='item-card__title'>{title}</h5>
				<p className='item-card__article'>Article: {article}</p>
				<p className='item-card__color'>Color: {color}</p>
			</div>
			<div className='item-card__footer'>
				<span className='item-card__price'>{price} UAH</span>
				<div className='item-card__actions'>
					<Button text='Add to cart' onClick={() => addToCart(item)}/>
					<span className='item-card__actions__fav' onClick={() => {
						const isFav = handleFavorites(item)
						setIsFavorite(isFav)
					}}>
                        {starIcon(isFavorite ? '#ffc107' : '#ffffff')}
                    </span>
				</div>
			</div>
		</div>
	)
}

export default ItemCard;