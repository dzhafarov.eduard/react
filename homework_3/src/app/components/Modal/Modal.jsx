import React from 'react';
import './Modal.scss'

const Modal = (props) => {
	const {header, text, closeButton, actions, handleClose} = props
	return (
		<div className='modal__background'
		     onClick={(event) => event.currentTarget === event.target && handleClose()}>
			<div className='modal'>
				<div className='modal__header'>
					<h5>{header}</h5>
					{closeButton && <a className='modal__close' onClick={handleClose}/>}
				</div>
				<div className='modal__content'>
					<p>{text}</p>
				</div>
				<div className='modal__actions'>
					{actions}
				</div>
			</div>
		</div>
	)
}

export default Modal;