import React from 'react';
import {Link} from "react-router-dom";
import './Header.scss'
import CartMenuItem from "./components/CartMenuItem";

const Header = (props) => {

	const {cart} = props

	return (
		<header>
			<div className='container'>
				<nav className='navbar'>
					<a className='navbar__logo' href="/">Some Logo</a>
					<menu className='navbar__menu'>
						<li className='navbar__menu__item'>
							<Link to='/'>Main</Link>
						</li>
						<li className='navbar__menu__item'>
							<Link to='/favorites'>Favorites</Link>
						</li>
						<li className='navbar__menu__item'>
							<Link to='/cart'>
								<CartMenuItem cart={cart}/>
							</Link>
						</li>
					</menu>
				</nav>
			</div>
		</header>
	);
}

export default Header;