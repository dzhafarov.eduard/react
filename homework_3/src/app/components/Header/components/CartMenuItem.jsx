import React from 'react';
import {cartIcon} from "../../../assets/AppIcons";
import "./CartMenuItem.scss"

const CartMenuItem = (props) => {

	const {cart = []} = props

	return (
		<div className='navbar__menu__cart'>
			<span>Cart</span>
			{cartIcon()}
			{cart.length > 0 && <span className='navbar__menu__cart-badge'>{cart.length}</span>}
		</div>
	);
}

export default CartMenuItem;