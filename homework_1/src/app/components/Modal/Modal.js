import React, {Component} from 'react';
import PropTypes from 'prop-types'
import Button from "../Button/Button";
import './Modal.scss'

class Modal extends Component {
	static propTypes = {
		header: PropTypes.string,
		text: PropTypes.string,
		closeButton: PropTypes.bool,
		actions: PropTypes.object,
		handleClose: PropTypes.func
	}

	static defaultProps = {
		header: 'Title',
		text: 'Modal text',
		closeButton: true,
		actions: <Button text={'Ok'}/>
	}

	render() {
		const {header, text, closeButton, actions, handleClose} = this.props
		return (
			<div className='modal__background'
			     onClick={(event) => event.currentTarget === event.target && handleClose()}>
				<div className='modal'>
					<div className='modal__header'>
						<h5>{header}</h5>
						{closeButton && <a className='modal__close' onClick={handleClose}/>}
					</div>
					<div className='modal__content'>
						<p>{text}</p>
					</div>
					<div className='modal__actions'>
						{actions}
					</div>
				</div>
			</div>
		);
	}
}

export default Modal;