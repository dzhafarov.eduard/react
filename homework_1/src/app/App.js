import React, {Component} from 'react';
import Button from "./components/Button/Button";
import './App.scss';
import Modal from "./components/Modal/Modal";

class App extends Component {

	state = {
		modal: null
	}

	setModal(modalType) {
		switch (modalType) {
			case 'confirm':
				this.setState({
					modal: <Modal
						header={'Confirm modal'}
						text={'Are you confirm this action?'}
						handleClose={() => this.setModal(null)}
						actions={
							<>
								<Button backgroundColor={'#FF0000'} text={'Cancel'} onClick={() => this.setModal(null)}/>
								<Button backgroundColor={'#00FF00'} text={'Ok'} onClick={() => this.setModal(null)}/>
							</>
						}
					/>
				});
				break
			case 'info':
				this.setState({
					modal: <Modal
						header={'Information modal'}
						text={'This is modal just for showing info message.'}
						closeButton={false}
						handleClose={() => this.setModal(null)}
						actions={
							<Button backgroundColor={'#00FFFF'} text={'Ok'} onClick={() => this.setModal(null)}/>
						}
					/>
				});
				break
			default:
				this.setState({
					modal: null
				})
		}
	}

	render() {
		const {modal} = this.state
		return (
			<div className='App'>
				<div className={'control-buttons'}>
					<Button text={'Open first modal'} onClick={() => this.setModal('confirm')}/>
					<Button text={'Open second modal'} onClick={() => this.setModal('info')}/>
				</div>
				{modal}
			</div>
		)
	}
}

export default App;
